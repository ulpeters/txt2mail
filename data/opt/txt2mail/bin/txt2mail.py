#!/usr/bin/env python3
import os, shutil, smtplib, sys
from inotify_simple import INotify, flags
from email.mime.text import MIMEText

# Define and create folders if they don't exist
base  = '/opt/txt2mail/var/'
prep  = base + '1-prep/'
ready = base + '2-ready/'
done  = base + '3-done/'
os.makedirs(prep,  exist_ok=True)
os.makedirs(ready, exist_ok=True)
os.makedirs(done,  exist_ok=True)

# Register inotify event for files moved to the "ready" folder
inotify = INotify()
watch_flags = flags.MOVED_TO
wd = inotify.add_watch(ready, watch_flags)
print('Watching for files moved to :' + os.path.dirname(os.path.realpath(__file__)) + '/' + ready)

# Get events and take action
while True:
  events = inotify.read()
  for event in events:
    # Report file found and read lines
    print(ready + event.name)
    lines = open(ready + event.name).readlines()

    # Compose e-mail
    msg = MIMEText(''.join(lines[1:]))
    msg['Subject'] = ''.join(lines[0]).strip()
    msg['From'] = os.environ['MAIL_FROM']
    msg['To'] = os.environ['MAILTO']

    # Send e-mail
    s = smtplib.SMTP(host=os.environ['MAIL_RELAY_HOST'], port=os.environ['MAIL_PORT'])
    # s.set_debuglevel(1)
    s.starttls()
    s.login(os.environ['MAIL_USER'], os.environ['MAIL_PASSWORD'])
    s.sendmail(os.environ['MAIL_FROM'], os.environ['MAILTO'], msg.as_string())
    s.quit()

    # Move file to the "done" folder
    shutil.move(ready + event.name,done)
