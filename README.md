# txt2mail
### Purpose
Ships plain text files, e.g. logs or status reports via e-mail.

### Functionality
- This image sends the content of each text file moved from the folder `1-prep` to `2-ready` via e-mail.
- The first line of the text file is set as mail subject and all following lines as message body.
When done, the mail is moved to `3-done`.
- Files can be either moved manually or will be moved periodically
```
data/opt/txt2mail/var/
├── 1-prep
│   ├── 15min
│   │   └── testmail
│   ├── daily
│   ├── hourly
│   ├── manually
│   ├── monthly
│   └── weekly
├── 2-ready
└── 3-done
```

### Configuration
1. `cp .env.template .env`
2. Set your mail config in .env

### Usage
- `docker-compose up -d`
- Bind-mount `txt2mail/data/mail`
- Create a text file `1-prep` to be sent via mail
- Move the file to `2-ready`

### Remarks
- This container runs as uid/gid 1000
- The `mail/` folder is created on first startup if it doesn't exist
