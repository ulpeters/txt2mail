FROM python:alpine

WORKDIR  /opt/txt2mail
ENV HOME /opt/txt2mail

COPY ./data/opt/txt2mail/bin/requirements.txt ./bin/

RUN chown -R 1000:1000 . \
    && adduser -h /opt/txtmail -s /bin/sh -H -D -u 1000 txt2mail \
    && pip install --no-cache-dir -r ./bin/requirements.txt

VOLUME   /opt/txt2mail
